import { RouterModule, Routes } from '@angular/router';
import { HolaMundoComponent } from './components/hola-mundo/hola-mundo.component';
import { ContainerComponent } from './components/container/container.component';
import { RowMedidasComponent } from './components/row-medidas/row-medidas.component';
import  { OffsetComponent } from './components/offset/offset.component';
import {  HiddenComponent} from './components/hidden/hidden.component';

const APP_ROUTES: Routes = [
    { path: 'holaMundo', component: HolaMundoComponent },
    { path: 'container', component: ContainerComponent },
    { path: 'medidas', component: RowMedidasComponent },
    { path: 'offset', component: OffsetComponent },
    { path: 'hidden', component: HiddenComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'holaMundo' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);