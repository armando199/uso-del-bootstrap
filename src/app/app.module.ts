import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { HolaMundoComponent } from './components/hola-mundo/hola-mundo.component';
import { ContainerComponent } from './components/container/container.component';
import { RowMedidasComponent } from './components/row-medidas/row-medidas.component';
import { OffsetComponent } from './components/offset/offset.component';
import { HiddenComponent } from './components/hidden/hidden.component';

@NgModule({
  declarations: [
    AppComponent,
    HolaMundoComponent,
    ContainerComponent,
    RowMedidasComponent,
    OffsetComponent,
    HiddenComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
