import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowMedidasComponent } from './row-medidas.component';

describe('RowMedidasComponent', () => {
  let component: RowMedidasComponent;
  let fixture: ComponentFixture<RowMedidasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowMedidasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowMedidasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
